import { Component } from '@angular/core';
import {  FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Sort} from '@angular/material';

export interface D {
  name:string;
  phone: string;
  email: string;
 }
@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent {


 searchText;

  addForm=new FormGroup({
    name: new FormControl ('', [Validators.required]),
         phone:new FormControl ('', [Validators.required, Validators.maxLength(10)]),
        email:new FormControl ('', [Validators.required, Validators.email])
});
sortedData :D[]= [
  { name: "Geetha", phone: "8508995549", email: "ageethas777a@gmail.com" },
  { name: "Achu", phone: "8248304192", email: "achutruefriend@gmail.com" },
  { name: "Dharshini", phone: "9578008330", email: "dharsh@gmail.com" },
   { name: "Thala", phone: "9876543210", email: "thalaa@gmail.com" },
  // { name: "Guna", phone: "9750073706", email: "gunasundhari@gmail.com" },
  // { name: "Rithu", phone: "9876543210", email: "rithuk@gmail.com" },
  { name: "Tharaneesh", phone: "9234567889", email: "tharaneeee@gmail.com" },
];
  model: any = {};
  model2: any = {};
  myValue;
  addMember() {
    this.sortedData.push(this.model);
    this.model = {};
  
  }


  deleteMember(i) {
    console.log(i);
    this.sortedData.splice(i, 1);
  }
   // editMember(member,index) {
    // console.log('member', member);
    // this.model2 = member;
    // this.myValue = index;
    editMember(k) {
    this.model2.name = this.sortedData[k].name;
    this.model2.phone = this.sortedData[k].phone;
    this.model2.email = this.sortedData[k].email;
    this.myValue = k;
    console.log(this.model2.name);

  }
   updateMember(K) {
  //  let k = this.myValue;
  //    for (let i = 0; i < this.members.length; i++) {
  //      if (i == k) {
  //       this.members[i] = this.model2;
  //      this.model2 = {};
  //     }
  //   }
  
  this.sortedData[this.myValue]=this.model2;
 }
 sortedDataa: D[];
 constructor() {
   this.sortedData = this.sortedData.slice();
 }
 sortData(sort: Sort) {
   const data = this.sortedData.slice();
   if (!sort.active || sort.direction === '') {
     this.sortedData = data;
     return;
   }
   this.sortedData = data.sort((a, b) => {
     const isAsc = sort.direction === 'asc';
     switch (sort.active) {
       case 'name': return compare(a.name, b.name, isAsc);
            default: return 0;
     }
   });
 }
}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}