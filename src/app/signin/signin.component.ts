import { Component } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent {
  // show:boolean;
  // g()
  // {
  //   this.show=!this.show;
  // }
  signinForm = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });
  constructor(private formBuilder: FormBuilder, private routes: Router) { }


  onSubmit() {

    const localname = localStorage.getItem('firstName');
    const localpwd = localStorage.getItem('password');

    if (this.signinForm.value.firstName === localname && this.signinForm.value.password === localpwd ) {
      this.routes.navigate(['/dash']);
    }
    else {
      alert('invalid username and password');
    }


  }

}
