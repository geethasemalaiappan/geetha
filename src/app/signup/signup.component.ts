import { Component } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  registerForm = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  constructor(private formBuilder: FormBuilder, private routes: Router) { }
 
  onSubmit() {
    if (this.registerForm.valid) {
      this.routes.navigate(['signin']);

    
    localStorage.setItem('firstName', this.registerForm.value.firstName);
    localStorage.setItem('password', this.registerForm.value.password);
    
  }
}
}