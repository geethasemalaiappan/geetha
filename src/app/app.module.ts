import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { DashComponent } from './dash/dash.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list';
import { CrudComponent } from './crud/crud.component';
import { FormsModule } from '@angular/forms';
import { AddComponent } from './add/add.component';
import { OtherComponent } from './other/other.component';
import { SortComponent } from './sort/sort.component';
import {MatSortModule} from '@angular/material/sort';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { ArrayComponent } from './array/array.component';


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    DashComponent,
    CrudComponent,
    AddComponent,
    OtherComponent,
    SortComponent,
    ArrayComponent,
   
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    Ng2SearchPipeModule,
    ReactiveFormsModule,
    MatListModule,
    MatSortModule,
    MatCheckboxModule,
    RouterModule.forRoot([
      { path: 'signin', component: SigninComponent },
      { path: 'signup', component: SignupComponent },
      { path: 'dash', component: DashComponent },
      { path: 'crud', component: CrudComponent  },
       { path: 'sort', component: SortComponent  },
         { path:'',redirectTo:'/signup', pathMatch :'full'},
         { path: 'array', component: ArrayComponent  }
      
    ]),
    BrowserAnimationsModule
  ],
  
    
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
