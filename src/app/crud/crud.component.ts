// import { Component } from '@angular/core';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent {
  title = 'binding';
  members = [
    {name:"geetha",position:"man"},
    {name:"priya",position:"ager" } , 
    {name:"tharshini",position:"manager" }
  ];
 model:any={};
 model2:any={};
 
 addMember(){
   this.members.push(this.model);
   this.model={};
  }
 deleteMember(i){
   console.log(i);
  this.members.splice(i,1);
}
myValue;
editMember(k)
{
  this.model2.name=this.members[k].name;
  this.model2.position=this.members[k].position;
  this.myValue=k;
}
updateMember()
{
  let k=this.myValue;
  for(let i=0;i<this.members.length;i++)
  {
    if(i==k)
    {
      this.members[i]=this.model2;
      this.model2={};
    }
  }
}
}